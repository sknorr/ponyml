<!DOCTYPE ponyMap>
<book id="ponyml">
  <date type="auto" value="vcs-document-change"/>
  <credit name="Stefan Knorr" email="sknorr@suse.de" affiliation="SUSE"/>
  <legal see="http://creativecommons.org/licenses/by-sa/4.0/"/>
  <textmacros from="macros.xml"/>
  The <m n="ponyml-l"/><title/>
  Not Yet Giving Up on XML<subtitle/>
  <m n="book-desc"/><desc/>

  <preface from="preface.xml"/>
  <chapter from="introduction.xml"/>
  <chapter from="advanced.xml" scoped="editors">
    <sect from="advanced-markup.xml"/>
    <sect from="advanced-pseudo-markup.xml" scoped="writers editors"/>
  </chapter>
  <appendix from="appendix.xml"/>

</book>
